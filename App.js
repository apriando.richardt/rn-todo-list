import React, { Component } from 'react';
import { View, Text, StatusBar, TextInput, FlatList, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import AsyncStorage from '@react-native-async-storage/async-storage'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todo:'',
      todoData: [],
      editMode: false,
      index:-1
    };
  }

  componentDidMount() {
    this.getData()
  }

  check = (item, index) => {
    const {todoData} = this.state
    let allData = todoData
    let editItem = item
    if(editItem.status == 'selesai'){
      editItem.status = 'belum selesai'
    }else{
      editItem.status = 'selesai'
    }

    allData[index].status = editItem.status
    this.setState({
      todoData: allData
    },() => this.saveData()
  )}

  deleteItem = (index) =>{
    const {todoData} = this.state
    let allData = todoData
    allData.splice(index, 1)
    this.setState({todoData: allData}, () => this.saveData())
  }

  addData = () => {
    const {todoData, todo, editMode, index} = this.state
    let allData = todoData
    if(editMode){
      allData[index].name = this.state.todo
      this.setState({editMode: false})
    }else{
      allData.push({ name: todo, status: 'belum selesai'})
    }
    this.setState({todoData: allData, todo: ''}, () => this.saveData())
  }

  saveData = async () =>{
    const {todoData} = this.state
    try{
      await AsyncStorage.setItem('@data', JSON.stringify(todoData))
      // console.log('Data Berhasil Disimpan');
    }catch(e){
      console.log(e)
    }
  }

  getData = async () =>{
    try{
      let value = await AsyncStorage.getItem('@data')
      value = JSON.parse(value)

      if(value !== null)
      {
        this.setState({todoData: value})
      }
      // console.log('Data berhasil di ambil');
    }catch(e){
      console.log(e);
    }
  }

  render() {
    const { todo, todoData } = this.state
    return (
      <View style={{flex:1, backgroundColor:'#212121'}}>
        <StatusBar barStyle='light-content' backgroundColor="#272727"/>
        <View style={{
            justifyContent:'center', 
            alignItems:'center', 
            backgroundColor:'#303030',
            paddingVertical:15,
            elevation: 3,
            marginBottom: 10
          }}>
          <Text style={{color:'#FAFAFA'}}>Todo List</Text>
        </View>
        <FlatList
            data={todoData}
            renderItem={({item, index}) =>(
                <View style={{
                    backgroundColor:'#303030', 
                    marginHorizontal:20, 
                    borderRadius:3, 
                    paddingVertical:10, 
                    paddingHorizontal:10,
                    marginTop:10,
                    flexDirection:'row'
                  }}>
                  <TouchableOpacity style={{flex:1, justifyContent:'center'}}
                    onPress={() => this.setState({todo: item.name, index, editMode:true})}
                  >
                    <Text style={{color:'#FAFAFA'}}>{item.name}</Text>
                  </TouchableOpacity>
                  <TouchableOpacity style={{justifyContent:'center', marginLeft:5}} onPress={() => this.deleteItem(index)}>
                    <Icon 
                      name='trash-alt'
                      size={25} color="#900"
                      color='#FAFAFA' 
                    />
                  </TouchableOpacity>
                  <TouchableOpacity 
                    onPress={() => this.check(item, index)}
                    style={{justifyContent:'center', marginLeft:20}}
                  >
                    <Icon 
                      name={item.status == 'selesai' ? 'check-square' : 'square'} 
                      size={25} color="#900" 
                      color='#FAFAFA'
                    />
                  </TouchableOpacity>
                </View>
            )}
            keyExtractor={(item) => item.name}
        />
       
        <View style={{
          flexDirection:'row',
          marginBottom:20
        }}>
          <TextInput
              value={todo}
              onChangeText={(text) => this.setState({todo: text})}
              placeholder="Input Todo"
              placeholderTextColor="#fafafa"
              style={{
                backgroundColor:'#303030', 
                paddingHorizontal:10, 
                marginHorizontal:20, 
                color:'#FAFAFA',
                marginBottom:10,
                width: '70%'
              }}
          />
          <TouchableOpacity style={{
              backgroundColor:'#303030', 
              marginBottom:10,
              justifyContent:'center',
              alignItems:'center',
              paddingVertical:10,
              borderRadius:50,
              width:50,
              height:50,
            }}
            onPress={() => this.addData()}
            >
            <Text style={{color:'#f2f2f2'}}>+</Text>
          </TouchableOpacity>
        </View>
        
      </View>
    );
  }
}

export default App;
